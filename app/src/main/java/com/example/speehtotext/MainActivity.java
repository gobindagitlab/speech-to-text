package com.example.speehtotext;

import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

public
class MainActivity extends AppCompatActivity {

    private static final int REEQUET_CODE_INPUT_SPEECH = 1000;
    private android.widget.TextView textView;
    private
    android.widget.ImageButton speak_btn;

    @Override
    protected
    void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_main );

        textView=findViewById ( com.example.speehtotext.R.id.textView );
        speak_btn=findViewById ( com.example.speehtotext.R.id.imageButton );


        speak_btn.setOnClickListener ( new android.view.View.OnClickListener ( ) {
            @Override
            public
            void onClick ( android.view.View v ) {
                speak();

            }
        } );

    }

    private  void speak(){

        android.content.Intent intent=new android.content.Intent ( android.speech.RecognizerIntent.ACTION_RECOGNIZE_SPEECH );//ActionReconzationSpeeck
        intent.putExtra ( android.speech.RecognizerIntent.EXTRA_LANGUAGE_MODEL, android.speech.RecognizerIntent.LANGUAGE_MODEL_FREE_FORM );//extralangugaemodel
        intent.putExtra ( android.speech.RecognizerIntent.EXTRA_LANGUAGE, java.util.Locale.getDefault () );
        intent.putExtra ( android.speech.RecognizerIntent.EXTRA_PROMPT ,"Hi Speak Something" );



        try {
            startActivityForResult ( intent,REEQUET_CODE_INPUT_SPEECH );

        }catch ( Exception e ){
            android.widget.Toast.makeText ( getApplicationContext (), e.getMessage (),
                                            android.widget.Toast.LENGTH_SHORT ).show ();
        }



    }

    @Override
    protected
    void onActivityResult ( int requestCode , int resultCode ,
                            @androidx.annotation.Nullable android.content.Intent data ) {
        super.onActivityResult ( requestCode , resultCode , data );


        switch ( requestCode ){

            case REEQUET_CODE_INPUT_SPEECH :{
                if ( resultCode==RESULT_OK && null!=data ){
                    java.util.ArrayList<String> result=data.getStringArrayListExtra ( android.speech.RecognizerIntent.EXTRA_RESULTS );
                 textView.setText ( result.get ( 0 ) );
                }
                break;

            }

        }


    }
}
